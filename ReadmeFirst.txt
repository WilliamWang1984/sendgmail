1.
Login to your Gmail:

Click "Turn on access (not recommended)" for "Less secure app access" option
The status should show "On"

2.
In main.py:

Replace '<Your Gmail Username>' with your actual Gmail Username
Replace '<Your Gmail Password>' with your actual Gmail Password

Note: for the sake of security, it is recommended to register an new Gmail account 

3. 
In contacts.txt:
list your receivers as follows:
Name EmailAddress

4. 
In message.txt:
Customise Email content as needed

5. 
Copy and rename your attachment, default name is 'attachment.jpg'
In case you have another filetype, change 'attachment.jpg' in main.py
