'''
Methods and Implementations Courtesy of: 
https://www.freecodecamp.org/news/send-emails-using-code-4fcea9df63f/
'''

import smtplib

from string import Template

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

class Email:

    def __init__(self, mailUsername, mailPassword):
        # Create some members
        self.address = mailUsername
        self.password = mailPassword


    def get_contacts(self, filename):
        names = []
        emails = []
        with open(filename, mode='r', encoding='utf-8') as contacts_file:
            for a_contact in contacts_file:
                names.append(a_contact.split()[0])
                emails.append(a_contact.split()[1])
        return names, emails


    def read_template(self, filename):
        with open(filename, 'r', encoding='utf-8') as template_file:
            template_file_content = template_file.read()
        return Template(template_file_content)

    '''
    Send email for given receivers
    '''
    def send_email(self, contacts, subject, message, atthFilename):
        contactNames, emailAddresses = self.get_contacts(contacts)  # read contacts
        messageTemplate = self.read_template(message)

        # set up the SMTP server
        smtpHostAndPort = smtplib.SMTP(host='smtp.gmail.com', port=587)
        smtpHostAndPort.starttls()

        print('Email Sender Credential is:')
        print(self.address)
        print(self.password)

        smtpHostAndPort.login(self.address, self.password)

        # For each contact, send the email:
        for contactName, emailAddress in zip(contactNames, emailAddresses):
            msg = MIMEMultipart()  # create a message

            # add in the actual person name to the message template
            message = messageTemplate.substitute(RECEIVER_NAME=contactName.title())

            # Prints out the message body for our sake
            print(message)

            # setup the parameters of the message
            msg['From'] = self.address
            msg['To'] = emailAddress
            msg['Subject'] = subject

            # add in the message body
            msg.attach(MIMEText(message, 'plain'))

            # add attachment
            atthFilehandle = open("./"+atthFilename,"rb")

            insMIMEBase = MIMEBase('application','octet-stream')

            insMIMEBase.set_payload((atthFilehandle).read())
            encoders.encode_base64(insMIMEBase)
            insMIMEBase.add_header('Content-Disposition', "attachment; filename= %s" % atthFilename)

            # attach MIMEBase instance of 'attachment' to message
            msg.attach(insMIMEBase)

            # send the message via the server set up earlier.
            smtpHostAndPort.send_message(msg)
            del msg

        # Terminate the SMTP session and close the connection
        smtpHostAndPort.quit()
