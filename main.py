'''
Methods and Implementations Courtesy of: 
https://www.freecodecamp.org/news/send-emails-using-code-4fcea9df63f/
'''

from Email import Email

def main():
    print("An Example of SMTP Gmail")
    gmailUsername = '<Your Gmail Username>'
    gmailPassword = '<Your Gmail Password>'
    eMailer = Email(gmailUsername, gmailPassword)   
    print("Starting Send Gmails...")
    eMailer.send_email('contacts.txt', 'TestSMTPGmail', 'message.txt', 'attachment.jpg')

if __name__ == "__main__":
    main()
